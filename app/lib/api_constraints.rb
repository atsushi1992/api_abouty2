class ApiConstraints

	def initialize(options)
		@version = options[:version]
		@default = options[:default]
	end

	def matches?(req)
		@default || req.headers['Accept'].include?("application/waytaxi.push.api.v#{@version}")
	end
end