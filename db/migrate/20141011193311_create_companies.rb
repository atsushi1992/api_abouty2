class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
	  t.integer :company_id
	  t.string :name, limit:80
	  t.string :email, limit:50
	  t.string :cnpj, limit:20
	  t.string :nome_fantasia, limit:20
	  t.string :razao_social, limit:20
	  t.string :password, limit:8
	  t.string :number, limit:10
	  t.string :complement, limit:130
	  t.string :district, limit:80
	  t.string :cep, limit:15
	  t.string :city, limit:70
	  t.string :state, limit:70
	  t.string :country, limit:70

	  t.timestamps
    end

  end
end
