class AddCollumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :email, :string
    add_column :users, :celphone, :string
    add_column :users, :birthday, :date
    add_column :users, :gender, :string
    add_column :users, :phone, :string
    add_column :users, :street, :string
    add_column :users, :number, :string
    add_column :users, :complement, :string
    add_column :users, :district, :string
    add_column :users, :cep, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :country, :string
    add_column :users, :cpf, :string
    add_column :users, :rg_number, :integer
    add_column :users, :rg_expire_date, :date
    add_column :users, :mother_name, :string
  end
end
