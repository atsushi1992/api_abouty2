class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
	  t.integer :code
	  t.string :name, limit:80
	  t.string :email, limit:50
	  t.string :celphone, limit:12
	  t.date :birthday
	  t.string :gender, limit:10
	  t.string :phone, limit:12
      t.timestamps
    end

  end
end
