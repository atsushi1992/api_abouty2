# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141011204334) do

  create_table "companies", force: true do |t|
    t.integer  "company_id"
    t.string   "name",          limit: 80
    t.string   "email",         limit: 50
    t.string   "cnpj",          limit: 20
    t.string   "nome_fantasia", limit: 20
    t.string   "razao_social",  limit: 20
    t.string   "password",      limit: 8
    t.string   "number",        limit: 10
    t.string   "complement",    limit: 130
    t.string   "district",      limit: 80
    t.string   "cep",           limit: 15
    t.string   "city",          limit: 70
    t.string   "state",         limit: 70
    t.string   "country",       limit: 70
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "email"
    t.string   "celphone"
    t.date     "birthday"
    t.string   "gender"
    t.string   "phone"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "district"
    t.string   "cep"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "cpf"
    t.integer  "rg_number"
    t.date     "rg_expire_date"
    t.string   "mother_name"
  end

end
