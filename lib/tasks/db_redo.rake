# encoding: utf-8
namespace :db do
	desc "recreate your dev and test db completely"
	task :redo => [:drop_all_tables, :create, :migrate, :seed, 'test:load'] do
		puts "YOUR development AND test DATABASES HAVE BEEN COMPLETELY RECREATED!!!".white.on_blue
	end

	task drop_all_tables: :environment do
		query = <<-QUERY
     SELECT
       table_name
     FROM
       information_schema.tables
     WHERE
       table_type = 'BASE TABLE'
     AND
       table_schema NOT IN ('pg_catalog', 'information_schema');
		QUERY

		begin
			connection = ActiveRecord::Base.connection
			results    = connection.execute(query)

			tables = results.map { |line| table_name = line['table_name'] }.join ", "
			connection.execute "DROP TABLE IF EXISTS #{ tables } CASCADE;"
		rescue ActiveRecord::NoDatabaseError
			puts "database didn't exist yet 😃".yellow
		end
	end

end